# IoT Temperature project

> By __César Freire__ @ 2022-02-08


## Build local images

```
cd iot-temperature
$ docker build -t mqtt_client mqtt_client/
$ docker build -t rest_server rest_server/
```

## Deploy containers
```
$ docker run  -d --name redis redis:alpine
$ docker run  -d -v db:/usr/src/app/db --link redis --rm --name mqtt_client mqtt_client
$ docker run  -d -v db:/usr/src/app/db -p 8000:8000 --rm --link redis --name rest_server rest_server
```

## Test

curl localhost:8000


